import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TrigTester {

	public static final double error = 0.0001; // the acceptable error with the math library
	
	//Tan approaches pos and neg infinty for some special values 
	public static double INFIN = 10e10;
	public static double NEG_INFIN = 10e10;
	
	public TrigTester() {
		
	}
	
	@Test
	public void testRadToDegrees() {
		assertEquals(Math.toDegrees(0), Trig.radiansToDegrees(0), error); 
		assertEquals(Math.toDegrees(Math.PI / 2), Trig.radiansToDegrees(Math.PI / 2), error); 
		assertEquals(Math.toDegrees(Math.PI), Trig.radiansToDegrees(Math.PI), error); 
		assertEquals(Math.toDegrees(3 * Math.PI / 2), Trig.radiansToDegrees(3 * Math.PI / 2), error); 
		assertEquals(Math.toDegrees(2 * Math.PI), Trig.radiansToDegrees(2 * Math.PI), error);
	}
	
	@Test 
	public void testDegreesToRad() {
		assertEquals(Math.toRadians(0), Trig.degreesToRadians(0), error); 
		assertEquals(Math.toRadians(Math.PI / 2), Trig.degreesToRadians(Math.PI / 2), error); 
		assertEquals(Math.toRadians(Math.PI), Trig.degreesToRadians(Math.PI), error); 
		assertEquals(Math.toRadians(3 * Math.PI / 2), Trig.degreesToRadians(3 * Math.PI / 2), error); 
		assertEquals(Math.toRadians(2 * Math.PI), Trig.degreesToRadians(2 * Math.PI), error);
	}

	@Test
	public void testSin() {

		// test some edge conditions
		assertEquals(0, Trig.Sin(0), error);
		assertEquals(Math.sin(Math.PI * 2), Trig.Sin(Math.PI * 2), error);

		// test some more special values around the unit circle
		assertEquals(Math.sin(Math.PI / 6), Trig.Sin(Math.PI / 6), error);
		assertEquals(Math.sin(Math.PI / 4), Trig.Sin(Math.PI / 4), error);
		assertEquals(Math.sin(Math.PI / 3), Trig.Sin(Math.PI / 3), error);

		assertEquals(Math.sin(5 * Math.PI / 6), Trig.Sin(5 * Math.PI / 6), error);
		assertEquals(Math.sin(3 * Math.PI / 4), Trig.Sin(3 * Math.PI / 4), error);
		assertEquals(Math.sin(2 * Math.PI / 3), Trig.Sin(2 * Math.PI / 3), error);

		assertEquals(Math.sin(7 * Math.PI / 6), Trig.Sin(7 * Math.PI / 6), error);
		assertEquals(Math.sin(5 * Math.PI / 4), Trig.Sin(5 * Math.PI / 4), error);
		assertEquals(Math.sin(4 * Math.PI / 3), Trig.Sin(4 * Math.PI / 3), error);

		assertEquals(Math.sin(Math.PI / 2), Trig.Sin(Math.PI / 2), error);
		assertEquals(Math.sin(3 * Math.PI / 2), Trig.Sin(3 * Math.PI / 2), error);
		assertEquals(Math.sin(Math.PI), Trig.Sin(Math.PI), error);

		// test negative values
		assertEquals(Math.sin(Math.PI * -1 / 6), Trig.Sin(Math.PI * -1 / 6), error);
		assertEquals(Math.sin(Math.PI * -1 / 4), Trig.Sin(Math.PI * -1 / 4), error);
		assertEquals(Math.sin(Math.PI * -1 / 3), Trig.Sin(Math.PI * -1 / 3), error);

		assertEquals(Math.sin(5 * Math.PI * -1 / 6), Trig.Sin(5 * Math.PI * -1 / 6), error);
		assertEquals(Math.sin(3 * Math.PI * -1 / 4), Trig.Sin(3 * Math.PI * -1 / 4), error);
		assertEquals(Math.sin(2 * Math.PI * -1 / 3), Trig.Sin(2 * Math.PI * -1 / 3), error);

		assertEquals(Math.sin(7 * Math.PI * -1 / 6), Trig.Sin(7 * Math.PI * -1 / 6), error);
		assertEquals(Math.sin(5 * Math.PI * -1 / 4), Trig.Sin(5 * Math.PI * -1 / 4), error);
		assertEquals(Math.sin(4 * Math.PI * -1 / 3), Trig.Sin(4 * Math.PI * -1 / 3), error);

		assertEquals(Math.sin(Math.PI * -1 / 2), Trig.Sin(Math.PI * -1 / 2), error);
		assertEquals(Math.sin(3 * Math.PI * -1 / 2), Trig.Sin(3 * Math.PI * -1 / 2), error);
		assertEquals(Math.sin(Math.PI * -1), Trig.Sin(Math.PI * -1), error);

		// test some values larger than 2 PI
		assertEquals(Math.sin(Math.PI * 2 + Math.PI / 6), Trig.Sin(Math.PI * 2 + Math.PI / 6), error);
		assertEquals(Math.sin(Math.PI * 2 + Math.PI / 4), Trig.Sin(Math.PI * 2 + Math.PI / 4), error);
		assertEquals(Math.sin(Math.PI * 2 + Math.PI / 3), Trig.Sin(Math.PI * 2 + Math.PI / 3), error);

	}

	@Test
	public void testCos() {
		// test some edge conditions
		assertEquals(Math.cos(0), Trig.Cos(0), error);
		assertEquals(Math.cos(Math.PI * 2), Trig.Cos(Math.PI * 2), error);

		// test some more special values around the unit circle
		assertEquals(Math.cos(Math.PI / 6), Trig.Cos(Math.PI / 6), error);
		assertEquals(Math.cos(Math.PI / 4), Trig.Cos(Math.PI / 4), error);
		assertEquals(Math.cos(Math.PI / 3), Trig.Cos(Math.PI / 3), error);

		assertEquals(Math.cos(5 * Math.PI / 6), Trig.Cos(5 * Math.PI / 6), error);
		assertEquals(Math.cos(3 * Math.PI / 4), Trig.Cos(3 * Math.PI / 4), error);
		assertEquals(Math.cos(2 * Math.PI / 3), Trig.Cos(2 * Math.PI / 3), error);

		assertEquals(Math.cos(7 * Math.PI / 6), Trig.Cos(7 * Math.PI / 6), error);
		assertEquals(Math.cos(5 * Math.PI / 4), Trig.Cos(5 * Math.PI / 4), error);
		assertEquals(Math.cos(4 * Math.PI / 3), Trig.Cos(4 * Math.PI / 3), error);

		assertEquals(Math.cos(Math.PI / 2), Trig.Cos(Math.PI / 2), error);
		assertEquals(Math.cos(3 * Math.PI / 2), Trig.Cos(3 * Math.PI / 2), error);
		assertEquals(Math.cos(Math.PI), Trig.Cos(Math.PI), error);

		// test negative values
		assertEquals(Math.cos(Math.PI * -1 / 6), Trig.Cos(Math.PI * -1 / 6), error);
		assertEquals(Math.cos(Math.PI * -1 / 4), Trig.Cos(Math.PI * -1 / 4), error);
		assertEquals(Math.cos(Math.PI * -1 / 3), Trig.Cos(Math.PI * -1 / 3), error);

		assertEquals(Math.cos(5 * Math.PI * -1 / 6), Trig.Cos(5 * Math.PI * -1 / 6), error);
		assertEquals(Math.cos(3 * Math.PI * -1 / 4), Trig.Cos(3 * Math.PI * -1 / 4), error);
		assertEquals(Math.cos(2 * Math.PI * -1 / 3), Trig.Cos(2 * Math.PI * -1 / 3), error);

		assertEquals(Math.cos(7 * Math.PI * -1 / 6), Trig.Cos(7 * Math.PI * -1 / 6), error);
		assertEquals(Math.cos(5 * Math.PI * -1 / 4), Trig.Cos(5 * Math.PI * -1 / 4), error);
		assertEquals(Math.cos(4 * Math.PI * -1 / 3), Trig.Cos(4 * Math.PI * -1 / 3), error);

		assertEquals(Math.cos(Math.PI * -1 / 2), Trig.Cos(Math.PI * -1 / 2), error);
		assertEquals(Math.cos(3 * Math.PI * -1 / 2), Trig.Cos(3 * Math.PI * -1 / 2), error);
		assertEquals(Math.cos(Math.PI * -1), Trig.Cos(Math.PI * -1), error);

		// test some values larger than 2 PI
		assertEquals(Math.cos(Math.PI * 2 + Math.PI / 6), Trig.Cos(Math.PI * 2 + Math.PI / 6), error);
		assertEquals(Math.cos(Math.PI * 2 + Math.PI / 4), Trig.Cos(Math.PI * 2 + Math.PI / 4), error);
		assertEquals(Math.cos(Math.PI * 2 + Math.PI / 3), Trig.Cos(Math.PI * 2 + Math.PI / 3), error);
	}

	@Test
	public void testTan() {
		
		/*
		 * For Tan some values (multiples of pi/2) approach neg or pos infinity 
		 * Rather than asserting equals were simply test if they approach a sufficiently 
		 * large value, this is to mimic Math.Tan()   
		 */
		
		// test some edge conditions
		assertEquals(0, Trig.Tan(0), error);
		assertEquals(Math.tan(Math.PI * 2), Trig.Tan(Math.PI * 2), error);

		// test some more special values around the unit circle
		assertEquals(Math.tan(Math.PI / 6), Trig.Tan(Math.PI / 6), error);
		assertEquals(Math.tan(Math.PI / 4), Trig.Tan(Math.PI / 4), error);
		assertEquals(Math.tan(Math.PI / 3), Trig.Tan(Math.PI / 3), error);

		assertEquals(Math.tan(5 * Math.PI / 6), Trig.Tan(5 * Math.PI / 6), error);
		assertEquals(Math.tan(3 * Math.PI / 4), Trig.Tan(3 * Math.PI / 4), error);
		assertEquals(Math.tan(2 * Math.PI / 3), Trig.Tan(2 * Math.PI / 3), error);

		assertEquals(Math.tan(7 * Math.PI / 6), Trig.Tan(7 * Math.PI / 6), error);
		assertEquals(Math.tan(5 * Math.PI / 4), Trig.Tan(5 * Math.PI / 4), error);
		assertEquals(Math.tan(4 * Math.PI / 3), Trig.Tan(4 * Math.PI / 3), error);

		assertTrue(Math.tan(Math.PI / 2) > TrigTester.INFIN);
		assertTrue(Math.tan(3 * Math.PI / 2) > TrigTester.INFIN);
		assertEquals(Math.tan(Math.PI), Trig.Tan(Math.PI), error);

		// test negative values
		assertEquals(Math.tan(Math.PI * -1 / 6), Trig.Tan(Math.PI * -1 / 6), error);
		assertEquals(Math.tan(Math.PI * -1 / 4), Trig.Tan(Math.PI * -1 / 4), error);
		assertEquals(Math.tan(Math.PI * -1 / 3), Trig.Tan(Math.PI * -1 / 3), error);

		assertEquals(Math.tan(5 * Math.PI * -1 / 6), Trig.Tan(5 * Math.PI * -1 / 6), error);
		assertEquals(Math.tan(3 * Math.PI * -1 / 4), Trig.Tan(3 * Math.PI * -1 / 4), error);
		assertEquals(Math.tan(2 * Math.PI * -1 / 3), Trig.Tan(2 * Math.PI * -1 / 3), error);

		assertEquals(Math.tan(7 * Math.PI * -1 / 6), Trig.Tan(7 * Math.PI * -1 / 6), error);
		assertEquals(Math.tan(5 * Math.PI * -1 / 4), Trig.Tan(5 * Math.PI * -1 / 4), error);
		assertEquals(Math.tan(4 * Math.PI * -1 / 3), Trig.Tan(4 * Math.PI * -1 / 3), error);

		assertTrue(Math.tan(Math.PI * -1 / 2) < TrigTester.NEG_INFIN);
		assertTrue(Math.tan(3 * Math.PI * -1 / 2) < TrigTester.NEG_INFIN);
		assertEquals(Math.tan(Math.PI * -1), Trig.Tan(Math.PI * -1), error);

		// test some values larger than 2 PI
		assertEquals(Math.tan(Math.PI * 2 + Math.PI / 6), Trig.Tan(Math.PI * 2 + Math.PI / 6), error);
		assertEquals(Math.tan(Math.PI * 2 + Math.PI / 4), Trig.Tan(Math.PI * 2 + Math.PI / 4), error);
		assertEquals(Math.tan(Math.PI * 2 + Math.PI / 3), Trig.Tan(Math.PI * 2 + Math.PI / 3), error);
	}

}
