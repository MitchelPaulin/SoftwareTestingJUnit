
public class Trig {

	private static final int accuracy = 100; // how many iterations to perform, increase this number if higher accuracy is desired 
	private static final double RAD_TO_DEG_CON = 180.0 / Math.PI; //constant for conversion 
	
	/*
	 * A Sin implementation based on the Taylor series expansion 
	 */
	public static double Sin(double radians) {
		double ans = 0;
		double denominator = 1;
		double numerator = radians;
		int fact_counter = 1;

		for (int iterations = 0; iterations < accuracy; iterations++) {

			if (iterations % 2 == 0) {
				ans += (numerator / denominator);
			} else {
				ans -= (numerator / denominator);
			}
			numerator *= radians * radians;
			denominator *= (fact_counter + 1) * (fact_counter + 2);
			fact_counter += 2;
		}
		return ans;
	}
	
	/*
	 * A Cos implementation based on the Taylor series expansion 
	 */
	public static double Cos(double radians) {
		double ans = 1;
		double denominator = 2;
		double numerator = radians * radians;
		int fact_counter = 2;

		for (int iterations = 0; iterations < accuracy; iterations++) {

			if (iterations % 2 == 1) {
				ans += (numerator / denominator);
			} else {
				ans -= (numerator / denominator);
			}
			numerator *= radians * radians;
			denominator *= (fact_counter + 1) * (fact_counter + 2);
			fact_counter += 2;
		}
		return ans;
	}
	
	/*
	 * For Tan some values (multiples of pi/2) approach neg or pos infinity 
	 * In these cases Tan will return a sufficiently large or small value 
	 */
	public static double Tan(double radians) {
		return Sin(radians) / Cos(radians);
	}
	
	/*
	 * A Sin implementation for degrees 
	 */
	public static double Sin_Deg(double degrees) {
		return radiansToDegrees(Sin(degreesToRadians(degrees)));
	}

	/*
	 * A Cos implementation for degrees
	 */
	public static double Cos_Deg(double degrees) {
		return radiansToDegrees(Cos(degreesToRadians(degrees)));
	}

	/*
	 * A Tan implementation for degrees 
	 */
	public static double Tan_Deg(double degrees) {
		return radiansToDegrees(Tan(degreesToRadians(degrees)));
	}
	
	/*
	 * Helper method for converting radians to degrees 
	 */
	public static double radiansToDegrees(double radians) {
		return radians * Trig.RAD_TO_DEG_CON;
	}
	
	/*
	 * Helper method for converting degrees to radians 
	 */
	public static double degreesToRadians(double degrees) {
		return degrees / Trig.RAD_TO_DEG_CON;
	}

}
